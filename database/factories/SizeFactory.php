<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Size>
 */
class SizeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
           
            'name' => fake()->name(),
            'product_id'=>fake()->name(),
            'product_category'=>fake()->name(),
            'is_active'=>fake()->boolean()
            // 'image' => fake()->image('public/assets/images',400,300, null, false) 
          
        ];
    }
}
