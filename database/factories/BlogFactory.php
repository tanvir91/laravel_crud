<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Blog>
 */
class BlogFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'name' => fake()->name(),
            'description'=>fake()->text(100),
            
            // 'image' => fake()->image('public/assets/images',400,300, null, false) 
           'image'=> fake()->imageUrl(640, 480, 'animals', true),
           'is_active'=>fake()->boolean()
        ];
    }
}
