<?php

namespace Database\Seeders;

use App\Models\category;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class categorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('categories')->insert([
            'name' => Str::random(10),
            'brand' => Str::random(10),
            'is_active'=> true,
             'created_at' =>Carbon::now(),
             'updated_at' =>Carbon::now()

        ]);

        category::create([
            'name' => Str::random(10),
            'brand' => Str::random(10),
            'is_active'=> true
        ]);
    }
}
