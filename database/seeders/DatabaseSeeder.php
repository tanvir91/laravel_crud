<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;


use Illuminate\Database\Seeder;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // DB::table('categories')->insert([
        //     'name' => Str::random(10),
        //     'brand' => Str::random(10),
        //     'is_active'=> true,
        //      'created_at' =>Carbon::now(),
        //      'updated_at' =>Carbon::now()

        // ]);
        // category::create([
        //     'name' => Str::random(10),
        //     'brand' => Str::random(10),
        //     'is_active'=> true
        // ]);

        $this->call([
            categorySeeder::class,
            ProductSeeder::class,
            SizeSeeder::class,
            ColorSeeder::class,
            BlogSeeder::class


        ]);
        \App\Models\User::factory(10)->create();
    }
}
