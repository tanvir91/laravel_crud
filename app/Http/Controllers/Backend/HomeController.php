<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view("backend/index");
    }

    public function productlist()
    {
        $products = Product::all();
        // dd($products);
        return view('backend.productlist', compact('products'));
    }


    public function productadd()
    {
        return view("backend/productadd");
    }


    public function productedit($id)
    {
        return view("backend/productedit");
    }
    public function productshow($id)
    {
        $prductShow = Product::find($id);
        return view('backend.productshow', compact('prductShow'));
    }
    public function productDelete($id)
    {
        $prduct = Product::find($id);
        $prduct->delete();

        // Session::flash('message', 'Deleted Successfully!');

        // return redirect()
        //       ->route('categories.index')
        //       ->with('message', 'Deleted Successfully!');

        return redirect()
            ->route('admin.productlist')
            ->withMessage('Deleted Successfully!');
    }

    public function category()
    {
        $categories = category::all();
        return view('backend.category', compact('categories'));
    }

    public function categoryadd()
    {

        return view("backend.categoryadd");
    }
    public function categoryshow($id)
    {
        $categories = category::find($id);
        return view('backend.categoryshow', compact('categories'));
    }



    public function categoryUpdate($id)
    {
        $categories = category::find($id);
        return view('backend/categoryedit', compact('categories'));
    }


    public function categoryStore(Request $request)
    {
        category::create([
            'name'=>$request->name,
            'brand'=>$request->brand
        ]);
        return view('backend/categoryadd');
    }
    public function categoryCreate()
    {

        return view('backend/categoryadd');
    }
    public function categoryedit($id)
    {
        $categories = category::find($id);
        return view('backend/categoryedit', compact('categories'));
    }
    public function categoryDelete($id)
    {
        $category = category::find($id);
        $category->delete();
        return redirect()
            ->route('admin.category')
            ->withMessage('Deletd Successfully');
    }
    public function userlist()
    {
        $users = User::all();
        return view('backend.userlist', compact('users'));
    }
    public function usershow($id)
    {
        $users = User::find($id);
        return view('backend.usershow', compact('users'));
    }
    public function userDelete($id)
    {
        $users = User::find($id);
        $users->delete();
        return redirect()->route('admin.userlist')
            ->withMessage('Deletd Successfully');
    }
    public function login()
    {
        return view("backend/login");
    }
}
