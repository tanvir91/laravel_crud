<x-admin.master>
    <!-- partial -->
    <x-slot:title>

        CATEGORY LIST
        </x-slot>

        <!-- Main Wrapper -->

        <div class="main-panel">
            <div class="content">
                <div class="container-fluid">
                    <h4 class="page-title">Category List</h4>
                    <div class="row">
                        <div class="col-md-10">

                            <div class="main-content bg-dark">
                                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                                    <h1 class="h2">User</h1>
                                    <div class="btn-toolbar mb-2 mb-md-0">
                                        <div class="btn-group me-2">
                                            <button type="button" href="" class="btn btn-sm btn-outline-secondary">create </button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary">Export Excel</button>
                                        </div>
                                        <button type="button" href="{{route('admin.categoryCreate')}}" class="btn btn-sm btn-outline-primary">
                                            <span data-feather="plus"></span>
                                            Create New
                                        </button>
                                    </div>
                                </div>
                                <a href="{{route('admin.categoryCreate')}}"><button type="submit" class="btn btn-info w-100">create Product</button></a>


                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body  text-center">
                                            <div class=" w-100 ">
                                                <div class=" px-2">
                                                    @if(session('message'))
                                                    <p class="text-success">
                                                        {{ session('message') }}
                                                    </p>
                                                    @endif
                                                    <label for="caregory" class="mb-4 h1 text-dark">List Category</label>
                                                    <table class="table table table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Sr</th>
                                                                <th scope="col">Id</th>
                                                                <th scope="col">Name</th>
                                                                <th scope="col">Brand</th>
                                                                <th scope="col">Status</th>
                                                                <th scope="col">Image</th>
                                                                <th colspan="3" scope="col">Action</th>

                                                        </thead>
                                                        <tbody>
                                                            @foreach ($categories as $category)
                                                            <tr>
                                                                <th scope="row">{{$loop->iteration}}</th>
                                                                <td>{{$category->id }}</td>
                                                                <td>{{$category->name }}</td>
                                                                <td>{{$category->brand }}</td>
                                                                <td>{{$category->is_active ? 'yes' : 'no'}} </td>
                                                                <td> </td>
                                                                <td><a href="{{route('admin.categoryshow',$category->id)}}"><button type="submit" class="btn btn-info w-100">Show</button></a></td>
                                                                <td><a href="{{route('admin.categoryedit',$category->id)}}"><button type="submit" class="btn btn-primary w-100">Edit</button></a></td>
                                                                <td><a href="{{route('admin.categoryDelete',$category->id)}}"><button type="submit" class="btn btn-primary w-100">Delete</button></a></td>

                                                            </tr>
                                                            @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>



                </div>
            </div>

            <x-admin.partials.footer />

        </div>
        </x-admin>
        <!-- Main Wrapper -->