<x-admin.master>
    <!-- partial -->
    <x-slot:title>

        PRODUCT LIST
        </x-slot>

        <!-- Main Wrapper -->

        <div class="main-panel">
            <div class="content">
                <div class="container-fluid">
                    <h4 class="page-title">Dashboard</h4>
                    <div class="row">
                        <div class=" my-container active-cont">
                            <!-- Top Nav -->

                            <!--End Top Nav -->

                            <br>
                            <br>
                            <div class="col-md-10">
                                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                                    <h1 class="h2">User</h1>
                                    <div class="btn-toolbar mb-2 mb-md-0">
                                        <div class="btn-group me-2">
                                            <button type="button" class="btn btn-sm btn-outline-secondary">Export PDF</button>
                                            <button type="button" class="btn btn-sm btn-outline-secondary">Export Excel</button>
                                        </div>
                                        <button type="button" class="btn btn-sm btn-outline-primary">
                                            <span data-feather="plus"></span>
                                            Add New
                                        </button>
                                    </div>
                                </div>
                                <div class="main-content">


                                    <div class="col-md-12 mx-5">
                                        <div class="card">
                                            <div class="card-body  text-center">
                                                <div class=" w-100 ">
                                                    <div class=" px-2">
                                                        @if(session('message'))
                                                        <p class="text-success">
                                                            {{ session('message') }}
                                                        </p>
                                                        @endif
                                                        <label for="caregory" class="mb-4 h1 text-dark">List User</label>
                                                        <table class="table table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">SR</th>
                                                                    <th scope="col">ID</th>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Email</th>
                                                                    <th colspan="2" scope="col">Action</th>

                                                            </thead>
                                                            <tbody>
                                                                @foreach($users as $user)
                                                                <tr>
                                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                                    <td>{{$user->id }}</td>
                                                                    <td>{{$user->name }}</td>
                                                                    <td>{{$user->email }}</td>
                                                                    <td><a href="{{route('admin.usershow',$user->id)}}"><button type="submit" class="btn btn-info w-100">Show</button></a></td>
                                                                    <td><a href="{{route('admin.userDelete',$user->id)}}"><button type="submit" class="btn btn-danger w-100">Delete</button></a></td>

                                                                </tr>
                                                                @endforeach

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>



                </div>
            </div>

            <x-admin.partials.footer />

        </div>
        </x-admin>
        <!-- Main Wrapper -->