<x-admin.master>
    <!-- partial -->
    <x-slot:title>

        PRODUCT LIST
        </x-slot>

        <!-- Main Wrapper -->

        <div class="main-panel">
            <div class="content">
                <div class="container-fluid">
                    <h4 class="page-title">Dashboard</h4>
                    <div class="row">
                        <div class=" my-container active-cont mx-5">
                            <!-- Top Nav -->

                            <!--End Top Nav -->

                            <br>
                            <br>
                            <div class="col-md-12">

                                <div class="main-content bg-dark">

                                @if(session('message'))
                                                        <p class="text-success">
                                                            {{ session('message') }}
                                                        </p>
                                                        @endif
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body text-center">
                                                <div class=" w-100 ">
                                                    <div class=" px-2">
                                                        <label for="caregory" class="mb-4 h1 text-light">Product LIst</label>
                                                     
                                                        <table class="table table-responsive table-hover table-responsive round">
                                                            <thead>
                                                                <tr class="text-light">
                                                                    <th scope="col">Sr</th>
                                                                    <th scope="col">Product Id</th>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Title</th>
                                                                    <th scope="col">Description</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Image</th>
                                                                    <th colspan="3" scope="col">Action</th>

                                                            </thead>
                                                            <tbody>

                                                                @foreach ($products as $product)
                                                                <tr>
                                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                                    <td>{{ $product->id}}</td>
                                                                    <td>{{ $product->name}}</td>
                                                                    <td>{{ $product->title}}</td>
                                                                    <td>{{ $product->description}}</td>
                                                                    <td>{{ $product->price}}</td>
                                                                    <td> <img src="{{ $product->image}}" height="40" alt=""></td>
                                                                    <td><a href="{{route('admin.productshow',$product->id)}}" class="text-light"><button type="submit" class="btn btn-info w-100">Show</button></a></td>
                                                                    <td><a href="{{route('admin.productedit',$product->id)}}" class="text-light"><button type="submit" class="btn btn-primary w-100">Edit</button></a></td>
                                                                    <td><a href="{{route('admin.productDelete',$product->id)}}" class="text-light"><button type="submit" class="btn btn-primary w-100">Delete</button></a></td>

                                                                    
                                                                </tr>
                                                                @endforeach


                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>



                </div>
            </div>

            <x-admin.partials.footer />

        </div>
        </x-admin>